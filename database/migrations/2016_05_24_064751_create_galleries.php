<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("galleries",function($table)
        {
            $table->increments("id");
            $table->integer("user_id");
            $table->integer("category_id");
            $table->string("description")->nullable();
            $table->string("url");
            $table->datetime("approved")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("galleries");
    }
}
