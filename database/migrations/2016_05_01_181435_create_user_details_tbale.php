<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTbale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("user_details",function($table)
        {
            $table->increments("id");
            $table->integer("user_id");
            $table->string("f_name");
            $table->string("l_name")->nullable();
            $table->boolean("gender");
            $table->string("avatar")->nullable();
            $table->integer("user_type_id");
            $table->text("about_you")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("user_details");
    }
}
