<?php

use Illuminate\Database\Seeder;
use App\User;
use App\UserDetail;
class UserDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker\Factory::create();
        User::truncate();
        UserDetail::truncate();
        //creating admin
        User::create([
        	'email'	=> 'admin@example.com',
    		'user_type' => 5,
    		'allowed' => 1,
    		'confirm' => 1,
    		'password' => bcrypt(1),
            'id'        => 1
        ]);
        User::create([
            'email' => 'ahmar.siddiqui@gmail.com',
            'user_type' => 2,
            'allowed' => 1,
            'confirm' => 1,
            'password' => bcrypt(1),
            'id'    => 111
        ]);
        UserDetail::create([
            'f_name'    => 'ahmar',
            'l_name'    => 'siddiqui',
            'user_type_id' => 2,
            'gender'    => 1,
            'about_you' => 'something about me',
            'user_id'   => 111
        ]);
        UserDetail::create([
            'f_name'    => 'administrator',
            'user_type_id' => 5,
            'gender'    => 1,
            'about_you' => 'something about me',
            'user_id'   => 1
        ]);
        foreach(range(2,101) as $i)
        {
            $g=rand(1,2);
            $gender='male';
            if($g==2)
                $gender='female';

        	$user=User::create([
        		'email'	=> $faker->safeEmail,
        		'user_type' => rand(1,3),
        		'code' => md5(time().rand(10,15)),
        		'allowed' => rand(0,1),
        		'confirm' => rand(0,1),
        		'password' => bcrypt(1)
        	]);
        	UserDetail::create([
        		'f_name' => $faker->firstName($gender),
        		'l_name' => $faker->lastName($gender),
        		'gender' => $g,
        		'user_type_id' => $user->user_type,
        		'user_id' => $user->id,
        		'about_you' => $faker->paragraph(2)
        	]);

        }
    }
}
