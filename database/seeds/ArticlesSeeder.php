<?php

use Illuminate\Database\Seeder;
use App\Article;
use App\User;

class ArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker\Factory::create();
        Article::truncate();
        $user=User::paginate(10)->lists('id')->toArray();

        foreach(range(1,100) as $i)
        {
            $date=new Carbon\Carbon;
            $date->addMonth(rand(0,12));
            $date->addYear(rand(0,2));
        	$article=Article::create([
        		'user_id'	  => $user[array_rand($user)],
        		'approved'    => $date,
        		'topic'       => $faker->sentence(7),
                'description' => $faker->paragraph(10),
        		'link'        => $faker->url(),
                'url'         => 'docs/ce4c8fdc4a5dcfa73b47eddd324e131941458bos31154.pdf',
                'created_at'    => $date
        	]);
        }
    }
}
