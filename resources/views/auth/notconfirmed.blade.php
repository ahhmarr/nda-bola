@extends("master")
@section("content")
 <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="nda-gallery/banner2-02.jpg">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-orange font-36">Not confirmed</h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="/">Home</a></li>
                <li><a href="/about">About</a></li>
                <li class="active">Not confirmed</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

<!-- Start main-content -->
  <div class="main-content">
    
    <!-- Section: home -->
     <section id="home" class="parallax text-center layer-overlay overlay-white" data-bg-img="{{asset("nda-gallery/logo.png")}}">
      <div class="container pt-200 pb-200"> 
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <div class="display-table text-center">
              <div class="display-table-cell">
                <h2 class="text-orange font-weight-600 font-80">The E-mail address has not been verified yet</h1> 
                <h3 class="mt-0 mb-5">Please visit your inbox and click on the confirmation link.</h3>
                <p> If you don't see the verification email in your inbox, please check your Junk or Spam folders.
              	</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
@stop




