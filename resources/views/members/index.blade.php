@extends("master")
@section("content")
 <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="{{asset("nda-gallery/banner2-02.jpg")}}">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-orange font-36">
                Members
              </h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="{{asset("/")}}">Home</a></li>
                <li><a href="{{asset("/about")}}">About</a></li>
                <li class="active">Members</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

   
    <!-- Section: Volunteer -->
    <section class="divider parallax layer-overlay overlay-white">
      <div class="container pb-80">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h3 class="text-uppercase mt-0">{{isset($type) && $type?userType($type):'All'}} Members</h3>
              <div class="title-icon">
                <i class="flaticon-hand221"></i>
              </div>
              <p>
                <a class="btn btn-colored btn-orange btn-lg pull-left pl-30 pr-30" href="/members/1">Army</a>

                <a class="btn btn-colored btn-orange btn-lg pull-left pl-30 pr-30" href="/members/2">Navy</a>

                <a class="btn btn-colored btn-orange btn-lg pull-left pl-30 pr-30" href="/members/3">Air Force</a>
              </p>
            </div>
          </div>
        </div>
        <div class="section-content">
          {{$users->links()}}

          <div class="row multi-row-clearfix">
            @foreach($users as  $us)
              <div class="col-sm-6 col-md-3 mb-sm-60 text-left sm-text-center mb-30">
              <div class="volunteer border maxwidth400 p-30" data-bg-color="#fafafa">
                <div class="thumb"><a href="{{asset($us->details->avatar?:'img/user.png')}}"><img alt="" src="{{asset($us->details->avatar?:'img/user.png')}}" class="img-fullwidth"></a></div>
                <div class="info">
                  <h4 class="name">
                    <a href="#">{{full_name($us->details)}}</a>
                  </h4>
                  <h6 class="occupation">{{userType($us->details->user_type_id)}}</h6>
                  <p>
                    {{substr($us->details->about_you,0,40)}}
                  </p>
                  <hr>
                  
                </div>
              </div>
            </div>
            @endforeach
            {{$users->links()}}
          </div>
        </div>
      </div>
    

    </section>

   
  <!-- end main-content -->
  @stop