@extends("master")
@section("content")
 
 <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="{{asset("nda-gallery/banner2-02.jpg")}}">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-orange font-36">Members</h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="{{asset("/")}}">Home</a></li>
                <li><a href="{{asset("/about")}}">About</a></li>
                <li class="active">Members</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

   
 
    <!-- Section: Practice Area -->
    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-4">
              <div class="thumb">
                <div class="circular-image">
                <img src="images/photos/volunteer.jpg" alt="">
                </div>
              </div>
            </div>
            <div class="col-md-8">
              <h4 class="text-uppercase mt-0">Volunteer</h4>
              <div class="line-bottom"></div>
              <h5 class="name mt-30 mb-0">Mack Jakaria</h5>
              <h6 class="mt-5">Engineer</h6>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam vero expedita fugiat illo quasi doloremque, in unde omnis sint assumenda! Quaerat in, reprehenderit corporis voluptatum natus sequi reiciendis ullam. Quam eaque dolorum voluptates cupiditate explicabo.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt culpa dicta quaerat qui recusandae possimus placeat quidem ipsam voluptates similique libero tempore, labore quasi delectus vero alias, consectetur blanditiis eum maxime sunt accusantium ipsa doloribus reiciendis. Ea quod reprehenderit deserunt. Veritatis omnis similique tempora delectus a consequuntur, quis.  Adipisicing elit. Nesciunt culpa dicta quaerat qui recusandae possimus placeat quidem ipsam voluptates similique libero tempore, labore quasi delectus vero alias.</p>
              <ul class="social-icons small square list-inline mt-15 mb-0">
               <li><a class="bg-orange" href="#"><i class="fa fa-facebook text-white"></i></a></li>
               <li><a class="bg-orange" href="#"><i class="fa fa-skype text-white"></i></a></li>
               <li><a class="bg-orange" href="#"><i class="fa fa-twitter text-white"></i></a></li>
             </ul>
            </div>
          </div>
          <div class="row mt-30">
            <div class="col-md-4">
              <h4>Education</h4>
              <div class="line-bottom mb-30"></div>
              <div class="volunteer-address">
                <ul>
                  <li>
                    <div class="media border-bottom p-15 mb-20" data-bg-color="#f5f5f5">
                      <div class="media-left">
                        <i class="fa fa-book text-orange font-24 mt-5"></i>
                      </div>
                      <div class="media-body">
                        <h5 class="mt-0 mb-0">Education:</h5>
                        <p>Bachelor’s degree in Engineering<br>From Rutgers University, California.</p>
                      </div>
                    </div>
                  </li>



                </ul>
              </div>
            </div>

        <!-- <div class="row mt-30"> -->
            <div class="col-md-4">
              <h4>Address</h4>
              <div class="line-bottom mb-30"></div>
              <div class="volunteer-address">
                <ul>
                  <li>
                    <div class="media border-bottom p-15 mb-20" data-bg-color="#f5f5f5">
                      <div class="media-left">
                        <i class="fa fa-map-marker text-orange font-24 mt-5">
                        </i>
                      
                      </div>
                      
                      <div class="media-body">
                        <h5 class="mt-0 mb-0">Address:</h5>
                        <p>Village 856 Broadway <br/>New York</p>
                      </div>
                    </div>
                  </li>


                  
                </ul>
              </div>
            </div>     

            <!-- <div class="row mt-30"> -->
            <div class="col-md-4">
              <h4>Contact</h4>
              <div class="line-bottom mb-30"></div>
              <div class="volunteer-address">
                <ul>
                  <li>
                    <div class="media border-bottom p-15 mb-20" data-bg-color="#f5f5f5">
                      <div class="media-left">
                       <i class="fa fa-phone text-orange font-24 mt-5"></i>
                      </div>
                      <div class="media-body">
                        <h5 class="mt-0 mb-0">Contact:</h5>
                        <p><span>Phone:</span> +262 695 2601<br><span>Email:</span> you@yourdomain.com</p>                      </div>
                    </div>
                  </li>


                  
                  
                </ul>
              </div>
            </div>

            
            
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->


  @stop