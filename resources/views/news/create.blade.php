@extends("user.master")
@section("content")
<section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
              Create News
          </h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <!-- <i class="fa fa-minus"></i></button> -->
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <!-- <i class="fa fa-times"></i></button> -->
          </div>
        </div>
        {{Form::open(['role'=>'form','files'=>true,'route'=>'user.news.store'])}}
              <div class="box-body">
                <div class="form-group">
                  <label for="topic">Topic</label>
                  <input type="text" required name="topic" class="form-control" id="topic" placeholder="Enter Topic">
                </div>
                <div class="form-group">
                  <label for="link">Link To Article</label>
                  <input type="url" class="form-control" name="link" id="link" placeholder="Link To Article">
                </div>
                <div class="form-group">
                  <label for="file">File</label>
                  <input type="file" name="file" id="file" accept="application/pdf">

                  <!-- <p class="help-block">pdf </p> -->
                </div>
                <div class="form-group">
                  <label>Description</label>
                  <textarea required class="form-control" id="description" name="description" rows="7" placeholder="Enter ..."></textarea>
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
        <!-- /.box-body -->
        
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <script type="text/javascript">
      $(function(){
        $("#description").wysihtml5();
      });
    </script>
@stop