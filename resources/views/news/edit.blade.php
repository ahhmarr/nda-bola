@extends("user.master")
@section("content")
<section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
              Create News
          </h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <!-- <i class="fa fa-minus"></i></button> -->
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <!-- <i class="fa fa-times"></i></button> -->
          </div>
        </div>
        {!! Form::open(['role'=>'form','files'=>true,'route'=>['user.news.update',$new->id]]) !!}
          {{Form::hidden('_method','PUT')}}
              <div class="box-body">
                <div class="form-group">
                  <label for="topic">Topic</label>
                  <input type="text" required name="topic" class="form-control" value="{{$new->topic}}" id="topic" placeholder="Enter Topic">
                </div>
                <div class="form-group">
                  <label for="link">Link To Article</label>
                  <input type="url" class="form-control" name="link" id="link" placeholder="Link To Article" value="{{$new->link}}">
                </div>
                <div class="form-group">
                  <label for="file">File</label>
                  <input type="file" name="file" id="file" accept="application/pdf">
                @if($new->url)
                  <p class="help-block">
                    <a href="{{asset($new->url)}}">
                      {{explode('/', $new->url)[1]}}
                    </a>
                  </p>
                @endif
                </div>
                <div class="form-group">
                  <label>Description</label>
                  <textarea class="form-control" id="description" name="description" rows="7" placeholder="Enter ...">{!! $new->description !!}</textarea>
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
        <!-- /.box-body -->
        
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <script type="text/javascript">
      $(function(){
        $("#description").wysihtml5();
      });
    </script>
@stop