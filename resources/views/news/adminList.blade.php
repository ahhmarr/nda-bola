@extends("admin.master")
@section("content")
<section class="content">
	<div class="box">
            <div class="box-header">
              <h3 class="box-title">News</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody><tr>
                  <th style="width: 10px">#</th>
                  <th>topic</th>
                  <th>user</th>
                  <th>description</th>
                  <th>last updated</th>
                  <th>Approved</th>
                  <th>Action</th>
                </tr>
                <?php $x=0 ?>
                @foreach($news as  $new)
                	<tr>
                		<td>{{++$x}}</td>
                    <td>{{$new->topic}}</td>
                		<td>{{full_name($new->user->details)}}</td>
                		<td>{!!substr_close_tags($new->description,6)!!}</td>
                		<td>{{$new->updated_at->diffForHumans()}}</td>
                		<td>
                			@if($new->approved)
                			<a href="{{route('admin.news.approve',$new->id)}}">
                        <span class="badge bg-green">yes</span>  
                      </a>
                			@else
                      <a href="{{route('admin.news.approve',$new->id)}}">
                			<span class="badge bg-red">no</span>
                      </a>
                			@endif
                		</td>
                		<td>
                    <a target="_blank" href="{{route('admin.news.show',$new->id)}}">
                      View
                    </a>  
                    </td>
                	</tr>
                @endforeach
                
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>   

</section>
{{$news->links()}}
@stop