@include("site.head")
<body class="">
  <div id="wrapper">
    
    @include("site.header")
    
    <!-- Start main-content -->
    <div class="main-content">
      @yield("content")
    </div>
    
    @include("site.footer")
    <a class="scrollToTop" href="#">
      <i class="fa fa-angle-up"></i>
    </a>
  </div>
  <!-- end wrapper -->
  @include('site.foot')