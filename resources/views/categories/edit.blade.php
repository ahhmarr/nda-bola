<!-- modal start -->
{{Form::open(array("route"=>["user.categories.update",1],"class"=>"form-horizontal","id"=>"catEditForm"))}}
	{{Form::hidden("_method","PUT")}}
<div class="modal fade capitalCase">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h2 id="tempName">Edit Category</h2>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="editCat" class="col-xs-4 control-label">Category</label>
					<div class="col-xs-6">
						<input required type="text" name="category" id="editCat" class="form-control"/>
					</div>
				</div>
				<div class="form-group">
					<label for="editParent" class="col-xs-4 control-label">Parent</label>
					<div class="col-xs-6">
						<select name="parent" id="editParent" class="form-control">
				        <option value="">Parent</option>
				        @foreach($parents as  $parent)
				          <option value="{{$parent->id}}">{{$parent->category}}</option>
				        @endforeach
				      </select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-warning">update</button>
			</div>
		</div>
	</div>
</div>
{{Form::close()}}
<!-- modal ends -->
<script type="text/javascript">
	$(function(){
		$('.edit-cat').click(function()
		{
			var th=$(this);
			var cat=th.data('cat');
			var parent=th.data('parent');
			var action=th.data('route');
			$('#editCat').val(cat);
			$('#editParent').val(parent);
			$("#catEditForm").attr("action",action);
			$('.modal').modal('show');

		});
	});
</script>