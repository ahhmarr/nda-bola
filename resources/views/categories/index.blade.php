@extends("user.master")
@section("content")
<section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            <div class="col-xs-6">
              Categories
            </div>
            
          </h3>
          <div class="row">
            <div class="col-xs-9">
                @include('categories.create')
            </div>
            <div class="col-xs-6 col-md-9">
             <span class="pull-right">

             </span>
            </div>
          </div>
          <div class="box-tools pull-right">
            
              <!-- <i class="fa fa-minus"></i></button> -->
             
          </div>
        </div>
        <div class="box-body">
          <!-- Start creating your amazing application! -->
           <table class="table">
             <thead>
               <tr>
                 <th>#</th>
                 <th>category</th>
                 <th>parent</th>
                 <th>Action</th>
               </tr>
             </thead>
             <tbody>
               @foreach($cats as $cat)
                <tr>
                  <td>{{@++$x}}</td>
                  <td>{{$cat->category}}</td>
                  <td>
                    @if($cat->parent)
                      {{$cat->parent->category}}
                    @endif
                  </td>
                  <td>
                    <div class="btn-group">
                      <button class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                        Action <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li>
                          <a href="#" class="edit-cat"
                          data-cat="{{$cat->category}}" data-parent="{{$cat->parent_id}}" data-route="{{route('user.categories.update',$cat->id)}}">
                            <i class="fa fa-pencil"></i>  edit
                          </a>
                        </li>
                        <li>

                          <a class="confirm" href="{{route('user.categories.delete',$cat->id)}}">
                            <i class="fa fa-times"></i>   delete
                          </a>
                        </li>
                      </ul>
                    </div>
                  </td>
                </tr>
               @endforeach
             </tbody>
           </table>
        </div>
        <!-- /.box-body -->
        
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    @include('categories.edit')
@stop