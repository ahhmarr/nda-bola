{{Form::open(['route'=>'user.categories.store'])}}
    <div class="col-xs-4">
      <input required type="text" class="form-control" name="category" placeholder="category name">
    </div>
    <div class="col-xs-4">
      <select name="parent" class="form-control">
        <option value="">Parent</option>
        @foreach($parents as  $parent)
          <option value="{{$parent->id}}">{{$parent->category}}</option>
        @endforeach
      </select>
    </div>
    <div class="col-xs-3">
      <input type="submit" value="create" class="btn btn-success">  
    </div>
{{Form::close()}}