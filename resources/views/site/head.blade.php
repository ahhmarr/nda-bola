<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>

<!-- Meta Tags -->


<!-- Page Title -->
<title>Nigeria Defence Academey</title>



<!-- Stylesheet -->
<link href="{{asset("css/bootstrap.min.css")}}" rel="stylesheet" type="text/css">
<link href="{{asset("css/animate.css")}}" rel="stylesheet" type="text/css">
<link href="{{asset("css/css-plugin-collections.css")}}" rel="stylesheet"/>
<!-- CSS | menuzord megamenu -->
<link href="{{asset("css/menuzord-skins/menuzord-border-top.css")}}" rel="stylesheet"/>
<link href="{{asset("css/menuzord-skins/menuzord-border-top.css")}}" rel="stylesheet"/>

<!-- Revolution Slider -->
<link  href="{{asset("js/revolution-slider/css/settings.css")}}" rel="stylesheet" type="text/css"/>
<link  href="{{asset("js/revolution-slider/css/layers.css")}}" rel="stylesheet" type="text/css"/>
<link  href="{{asset("js/revolution-slider/css/navigation.css")}}" rel="stylesheet" type="text/css"/>

<!-- CSS | Custom Margin Padding Collection -->
<link href="{{asset("css/custom-bootstrap-margin-padding.css")}}" rel="stylesheet" type="text/css">
<!-- CSS | Main style file -->
<link href="{{asset("css/main-style.css")}}" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="{{asset("css/responsive.css")}}" rel="stylesheet" type="text/css">

<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="{{asset("css/style.css")}}" rel="stylesheet" type="text/css"> -->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>