@extends("master")
@section("content")
 <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="{{("nda-gallery/banner2-02.jpg")}}">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-orange font-36">Sign up</h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="{{asset("/")}}">Home</a></li>
                <li><a href="{{asset("/login")}}">Login</a></li>
                <li class="active">Register</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>


    <!-- Divider: Contact form -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <h4 class="text-uppercase mb-5">Join us</h4>
            <div class="line-bottom mb-20"></div>
            <p class="maxwidth600">Make a Difference. Sign up for Nigerian Defence Academy</p>
            <form id="donation-main-form" class="donation-form" method="POST" action="{{ url('/register') }}">
              {!! csrf_field() !!}
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group{{ $errors->has('f_name') ? ' has-error' : '' }}">
                    <label>First Name</label>
                    <input type="text" placeholder="First Name" name="f_name" value="{{ old('f_name') }}" required="" class="form-control">
                    @if ($errors->has('f_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('f_name') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" placeholder="Last Name" name="l_name" class="form-control" required="">
                  </div>
                </div>
                               <div class="col-sm-6">
                  <div class="form-group mb-20  {{ $errors->has('email') ? ' has-error' : '' }}" >
                   <label>E-mail</label>
                    <input type="email" placeholder="Enter your email" id="donate_cvc_number" name="email" class="form-control" required="">
                  
                  @if ($errors->has('email'))
                    <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                     </span>
                      @endif

                  </div>
                </div>

                                 <div class="col-sm-6">
                  <div class="form-group mb-20 {{ $errors->has('email') ? ' has-error' : '' }}" >
                    <label>Re-type Email</label>
                    <input type="email" placeholder="Re enter your Email" id="email" name="re-email" class="form-control" required="" value="{{ old('email') }}">
                     @if ($errors->has('email'))
                    <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                     </span>
                      @endif

                  </div>
                </div>

           

                  


                <div class="col-sm-6">
                  <div class="form-group mb-20">
                    <label>Gender</label>
                    <div class="radio mt-5">
                      <label class="radio-inline">
                        <input type="radio" value="1" name="gender">
                        Male</label>
                      <label class="radio-inline">
                        <input type="radio" value="2" name="gender">
                        Female</label>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group mb-20">
                    <label>Service Type</label>
                    <div class="radio mt-5">
                      <label class="radio-inline">
                        <input type="radio" value="1" name="service_type">
                        Army</label>
                      <label class="radio-inline">
                        <input type="radio" value="2" name="service_type">
                        Navy</label>
                      <label class="radio-inline">
                        <input type="radio" value="3" name="service_type">
                        Air Force</label>
                      
                    </div>
                  </div>
                </div>


              <div class="form-group">
                <label>About Yourself</label>
                <textarea rows="5" placeholder="Write About Yourself" id="about_yourself" name="about_yourself" required class="form-control"></textarea>
              </div>
              <div class="form-group text-center">
                <button data-loading-text="Please wait..." class="btn btn-colored btn-rounded btn-orange pl-30 pr-30" type="submit">Sign up</button> 
              </div>
               
            </form>
          </div>
        </div>
      </div>
    </section>
@stop