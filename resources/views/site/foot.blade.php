
<!-- jQuery scripts -->
<script src="{{asset("js/jquery-2.1.4.min.js")}}"></script> 
<script src="{{asset("js/bootstrap.min.js")}}"></script> 
<!-- Revolution Slider -->
<script src="{{asset("js/revolution-slider/js/jquery.themepunch.tools.min.js")}}"></script>
<script src="{{asset("js/revolution-slider/js/jquery.themepunch.revolution.min.js")}}"></script>
<script src="{{asset("js/custom-rev-slider.js")}}"></script> 
<!-- JS | google map -->
<script src="http://maps.google.com/maps/api/js?sensor=false"></script> 
<!-- JS | Necessary jQuery Collection for this theme --> 
<script src="{{asset("js/jquery-plugin-collection.js")}}"></script> 
<!-- JS | Custom script for all pages --> 
<script src="{{asset("js/custom.js")}}"></script>
</body>
</html>