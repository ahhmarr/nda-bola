<!-- Header -->
  <header class="header">
    <div class="header-nav navbar-fixed-top header-dark navbar-white navbar-transparent navbar-sticky-animated animated-active">
      <div class="header-nav-wrapper">
        <div class="container-fluid">
          <nav id="menuzord-right" class="menuzord orange">
            <a class="menuzord-brand" href="javascript:void(0)"><img src="{{asset("nda-gallery/logo.png")}}" alt=""></a>

            <a href="https://www.nda.edu.ng" class="pull-right menuzord-menu" style="color:#E44034;">visit the NDA website</a>
            
            <ul class="menuzord-menu">
              <li class="active"><a href="{{URL::to('/')}}">Welcome</a>
                
              </li>
              <li><a href="{{URL::to('/about')}}">About</a></li>
              
              <li><a href="{{url('/gallery')}}">NDA Gallery</a>
                
              </li>
              <li><a href="{{url('/members')}}">Members</a>
                <ul class="dropdown">
                  <li><a class="member-dropdown" href="{{url('/members/1')}}">Army</a>
                    
                  </li>
                  <li><a class="member-dropdown" href="{{url('/members/2')}}">Navy</a>
                    
                  </li>
                  <li><a class=href="{{url('/members/3')}}">Air Force</a></li>
                  
                </ul>
              </li>
              <li><a href="{{url('/article')}}">Articles</a>
              </li>
              <li><a href="{{URL::to('/contact')}}">Contact us</a>
                
              </li>
              <li ><a href="{{URL::to('/register')}}">Register</a>
                
              </li>
              <li><a href="{{URL::to('/login')}}">Login</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </header>