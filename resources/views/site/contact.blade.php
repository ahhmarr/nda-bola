@extends("master")
@section("content")
 <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="{{asset("nda-gallery/banner2-02.jpg")}}">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-orange font-36">Contact</h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="{{asset("/")}}">Home</a></li>
                <li><a href="{{asset("/about")}}">About</a></li>
                <li class="active">Contact</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>
    
    <!-- Divider: Google map -->
    <section id="contact" class="divider" data-bg-color="#fff">
      <div class="container pb-30">
        <div class="row">
          <div class="col-sm-5 col-md-3">
            <img src="{{asset("nda-gallery/logo.png")}}"  width="150" alt="">
          </div>
          <div class="col-sm-7 col-md-4 pl-60">
            <h4 class="text-uppercase">Get In Touch</h4>
            <div class="line-bottom mb-30"></div>
            <ul class="list-default footer-contact">
              <li><i class="fa fa-phone"></i>+234 810 660 5692</li>
              <li><i class="fa fa-envelope"></i>hello@yourdomain.com</li>
              <li><i class="fa fa-globe"></i>www.nda.edu.ng</li>
              <li><i class="fa fa-map-marker"></i>Nigerian Defence Academy, Kaduna, Kaduna State.</li>
            </ul>
            
          </div>
          <div class="col-sm-12 col-md-5">
            <h4 class="text-uppercase">Find Our Location</h4>
            <div class="line-bottom mb-30"></div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d62744.67968911403!2d7.3668071!3d10.6148991!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x383a932fd1457c20!2sNigerian+Defence+Academy!5e0!3m2!1sen!2sin!4v1462113042874" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Contact form -->
    <section>
      <div class="container pt-0">
        <div class="row">
          <div class="col-md-12">
            <h4 class="text-uppercase">Send Us a Messase</h4>
            <div class="line-bottom mb-30"></div>
            <form id="contact-form" class="contact-form-transparent mb-0 mb-sm-60">
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <input type="text" placeholder="Enter Name" id="contact_name" name="contact_name" required="" class="form-control">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <input type="text" placeholder="Enter Email" id="contact_email" name="contact_email" class="form-control" required="">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <input type="text" placeholder="Enter Subject" id="contact_subject" name="contact_subject" class="form-control" required="">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <textarea rows="5" placeholder="Enter Message" id="contact_message" name="contact_message" required class="form-control"></textarea>
              </div>
              <div class="form-group text-center">
                <button data-loading-text="Please wait..." class="btn btn-colored btn-rounded btn-orange pl-30 pr-30" type="submit">Send your message</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
@stop