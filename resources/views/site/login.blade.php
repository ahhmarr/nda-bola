@extends("master")
@section("content")

<!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="{{asset("nda-gallery/banner2-02.jpg")}}">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-orange font-36">About</h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="{{asset("/")}}">Home</a></li>
                <li><a href="{{asset("/register")}}">Sign up</a></li>
                <li class="active">Login</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

 <section id="home" class="parallax text-center layer-overlay overlay-white" data-bg-img="nda-gallery/armyslide4.jpg">
      <div class="container pt-200 pb-200">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <div class="display-table text-center">
              <div class="display-table-cell">
                <h2 class="text-orange font-weight-600 font-90">Login</h2>
                   <form class="form-horizontal" role="form" method="POST" action="{{ url('/authenticate') }}">
                        {!! csrf_field() !!}

                    <div class="form-group">
                       <div class="col-sm-12">
                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">

                      <input type="email" placeholder="Enter your Email" id="contact_email" name="email" class="form-control" required="" value="{{ old('email') }}">
                      
                      @if ($errors->has('email'))
                      <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                      </span>
                      @endif
                    
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                      <input type="password" placeholder="Enter Password" id="contact_subject" name="password" class="form-control" required="">
                    
                      @if ($errors->has('password'))
                      <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                      </span>
                      @endif
                    
                    </div>
                  </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                <button class="btn btn-default smooth-scroll" href="#"><i class="fa fa-sign-in font-16 text-orange mr-10"></i>login</button>
                <a class="btn btn-default smooth-scroll" href="{{ url('/password/reset') }}"><i class="fa fa-hand-o-right font-16 text-orange mr-10"></i>forget password</a>
                <div>
                   <a class="btn  btn-default smooth-scroll" href="#"><i class="fa  fa-facebook font-16 text-orange mr-10"></i>Login with facebook</a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@stop