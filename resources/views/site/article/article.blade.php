@extends("master")
@section("content")
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="{{asset("nda-gallery/banner2-02.jpg")}}">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-orange font-36">Article</h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li class="active">Article</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

    <!-- Divider: Partners & Donors -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-9">
            @foreach($articles as $article)
              <article class="post box-hover-effect effect1 clearfix mb-20 p-15 pl-0" data-bg-color="#f5f5f5">
              <div class="col-sm-4">
                <div class="entry-header">
                  <div class="post-thumb thumb"> <img class="img-responsive img-fullwidth" src="{{asset("images/project/1.jpg")}}" alt=""> </div>
                </div>
              </div>
              <div class="col-sm-8">
                <div class="entry-content mt-0 mt-xs-20">
                  <h4 class="entry-title text-uppercase font-weight-500 mt-0 pt-0"><a href="{{route('site.article.show',$article->id)}}">{{$article->topic}}</a></h4>
                  <ul class="list-inline font-12 mb-20 mt-10">
                    <li>
                      posted by 
                      <a href="#" class="text-orange">
                        @if($article->user && $article->user->details )
                          {{full_name($article->user->details)}}
                        @endif
                        |
                      </a>
                     </li>
                    <li><span class="text-orange">
                      {{$article->created_at->format('M d, y')}}
                    </span></li>
                  </ul>
                  <p class="mb-30">{!! substr_close_tags($article->description,100) !!}</p>
                 
                  <a href="{{route('site.article.show',$article->id)}}" class="text-orange pull-right">Details <i class="fa fa-angle-double-right"></i></a>
                </div>
              </div>
            </article>
            @endforeach
            
            <div class="row">
              <div class="col-sm-12">
                {{$articles->links()}}
              </div>
            </div>
          </div>
          @include('site.article.sidebar')
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
@stop