<div class="col-sm-12 col-md-3">
  <div class="sidebar maxwidth400 mt-sm-30">
    <div class="sidebar-widget mb-20">
      <div class="search-form">
        <form>
          <div class="input-group">
            <input type="text" class="form-control search-input" placeholder="Click to Search">
            <span class="input-group-btn">
            <button class="btn search-button" type="submit"><i class="fa fa-search"></i></button>
            </span>
          </div>
        </form>
      </div>
    </div>
    <div class="sidebar-widget p-15 pt-10 bg-solid-color">
      <h5 class="widget-title mt-0 mb-10">Archive <span class="line-bottom"></span></h5>
      <div class="categories">
        <ul class="list angle-right">
          @foreach($bars as $bar)
            <li>
              <a href="{{route('site.articles.index',['m'=>$bar->m,'y'=>$bar->y])}}">
                {{$bar->created_at->format('M Y')}}
                <span>({{$bar->total}})</span>
              </a>
            </li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
</div>