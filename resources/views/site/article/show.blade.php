@extends("master")
@section("content")

<!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="{{asset("nda-gallery/banner2-02.jpg")}}">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                  <h2 class="text-orange font-36">Article</h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li class="active">Article</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

    <section>
      <div class="container">
        <div class="row multi-row-clearfix blog-posts blog-style1">
          <div class="col-md-8">
            <div class="blog-posts single-post">
              <article class="post clearfix mb-0">
                <div class="entry-header">
                  <div class="clearfix"></div>
                  <div class="post-thumb"> <img alt="" src="{{asset("nda-gallery/vision.png")}}" class="img-responsive img-fullwidth"> </div>
                 
                </div>
                <div class="entry-title pt-0">
                  <h3><a href="#">{{$article->topic}}</a></h3>
                </div>
                <div class="entry-meta">
                  <ul class="list-inline">
                    <li>Posted: <span class="text-orange">{{$article->created_at->format('M d, y')}}</span></li>
                    <li>By: 
                      <span class="text-orange">
                        @if($article->user && $article->user->details )
                          {{full_name($article->user->details)}}
                        @endif  
                      </span>
                    </li>
                  </ul>
                </div>
                <div class="entry-content">
                 {!! $article->description !!}
                </div>
              </article>
              
              <div class="comments-area">
                @if($article->url)
                  file : <br>
                  <a download href="{{asset($article->url)}}">
                    {{substr($article->url, 42)}}
                  </a>
                @endif
              </div>
              <div class="comment-box mt-0 pt-30">
                <div class="row">
                  <div class="col-sm-12">
                </div>
                </div>
              </div>
            </div>
          </div>
            @include('site.article.sidebar')
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  @stop