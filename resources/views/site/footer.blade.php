<!-- Footer -->
  <footer class="footer pb-0"data-bg-color="#25272e">
    <div class="container pb-30">
      <div class="row">
        <div class="col-sm-6 col-md-3">
          <div class="footer-widget"> <img class="mt-0" alt="" src="{{asset("nda-gallery/logo.png")}}" width="150" >

          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="footer-widget">
            <h5 class="widget-title text-white">Pages<span class="line-bottom"></span></h5>
            <nav>
              <ul class="list angle-double-right">
                <li><a href="{{URL("/")}}">Welcome</a></li>
                <li><a href="{{URL("/about")}}">About</a></li>
                <li><a href="#">NDA Gallery</a></li>
                <li><a href="{{url('/members')}}">Members</a></li>
                <li><a href="{{URL("/contact")}}">Contact us</a></li>
                
              </ul>
            </nav>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="footer-widget">
            <h5 class="widget-title text-white pb-10">Quick Links<span class="line-bottom"></span></h5>
            <nav>
              <ul class="list angle-double-right">
                <li><a href="{{asset("/register")}}">Register</a></li>
                <li><a href="{{asset("/login")}}">Login</a></li>
            </nav>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="footer-widget">
            <h5 class="widget-title text-white">Quick Contact<span class="line-bottom"></span></h5>
            <ul>
              <li><a href="#">+(012) 345 6789</a></li>
              <li><a href="#"> info@nda38rc.org</a></li>
              <li><a class="lineheight-20" href="#">Nigerian Defence Academy, <br>
              Kaduna, <br>
              Kaduna State. <br>
              Nigeria
              </a></li>
            </ul>
          <!--   <p class="text-white">Subscribe Our Newsletter</p>
            <form id="footer-mailchimp-subscription-form" class="newsletter-form">
              <div class="input-group">
                <input type="email" class="form-control" value="" name="EMAIL" placeholder="Your Email"  class="form-control input-lg" data-height="43px" id="mce-EMAIL">
                <span class="input-group-btn">
                    <button class="btn btn-colored btn-orange btn-lg m-0" type="submit"><i class="fa fa-paper-plane-o text-white"></i></button>
                </span>
              </div> -->
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid p-20 bg-color">
      <div class="row text-center">
        <div class="col-md-12">
          <p class="font-11 text-white m-0">Copyright &copy;<strong style="color:#343434;"> www.nda38rc.org </strong></i> 2016. All Rights Reserved</p>
        </div>
      </div>
    </div>
  </footer>