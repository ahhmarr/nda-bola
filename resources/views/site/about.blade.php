@extends("master")
@section("content")
 <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="{{asset("nda-gallery/banner2-02.jpg")}}">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-orange font-36">About</h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="{{asset("/")}}">Home</a></li>
                <li><a href="{{asset("/login")}}">Login</a></li>
                <li class="active">About</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

    <!-- Section: About -->
    <section> 
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-7">
            <div class="image-carousel">
              <div class="item">
                <div class="thumb">
                  <img src="{{asset("nda-gallery/navy.jpg")}}" alt="">
                </div>
              </div>
              <div class="item">
                <div class="thumb">
                  <img src="{{asset("nda-gallery/navy.jpg")}}" alt="">
                </div>
              </div>
              <div class="item">
                <div class="thumb">
                  <img src="{{asset("nda-gallery/navy.jpg")}}" alt="">
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-5">
            <h3 class="text-orange text-uppercase">Nigerian Defence Academy</h3>
            <p><Strong><h4 class="mt-0">Our History</h4> </Strong></p><p>The Nigerian Defence Academy (NDA) was established on 5 February 1964 in response to the defence needs of independent Nigeria to train officers for the Armed Forces of Nigeria. Before then, the institution was known as the Royal Military Forces Training College (RMFTC). After independence in 1960, it became known as the Nigerian Military Training College.</p>

            <p>The role of the Academy is to provide each officer cadet with knowledge, skills and values necessary to meet the requirements of a military officer through military, academic and character development. In essence, the NDA is an institution where selected young able-bodied men and women are groomed into well educated, courageous, virile and erudite subalterns. </p>

            
            </div>
          </div>
        </div>
       
    
    <!-- divider: Featured News -->
    <section class="divider parallax layer-overlay overlay-white"  data-bg-img="nda-gallery/banner2.jpg">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-7">
              <h3 class="mt-0">Our Vision</h3>
              <div class="line-bottom mb-20"></div>
              <p class="mt-30 mb-30">The vision of the NDA is to produce officers with broad-based training in both military and academic subjects designed to serve as a foundation for the future progressive development of officers of the Nigerian Armed Forces.</p>
              
            </div>
            <div class="col-md-7">
              <h3 class="mt-0">Our Mission</h3>
              <div class="line-bottom mb-20"></div>
              <p class="mt-30 mb-30">The mission of NDA is to provide each officer cadet with the knowledge, skills and values necessary to meet the requirements of a military officer through military,academic and character development.</p>
              
            </div>
            
            
          </div>
        </div>
      </div>      
    </section>
@stop