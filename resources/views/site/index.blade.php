@extends("master")
@section("content")
<!-- Section: home -->
<section id="home" class="divider no-bg">
  <div class="container-fluid p-0">
    <div id="revolution-slider-fullscreen_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="concept1" style="background-color:#000000;padding:0px;">
      <!-- START REVOLUTION SLIDER 5.1.1RC fullscreen mode -->
      <div id="revolution-slider-fullscreen-bullet-bottom-right" class="rev_slider" style="display:none; height: 137px;" data-version="5.1.1RC">
        <ul>

          <!-- SLIDE 1 -->
          <li data-index="rs-1" data-transition="random" data-slotamount="7"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="nda-gallery/banner2.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-description="">
            <!-- MAIN IMAGE -->
            <img src="{{asset("nda-gallery/banner22.jpg")}}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
            
          </li>
        </ul>
        <div class="tp-bannertimer tp-top" style="height: 7px; background-color: rgba(0, 0, 0, 0.1);"></div>  
      </div>
    </div>
  </div>
</section>

   
<!-- divider: Became a Member -->
<section class="divider parallax layer-overlay overlay-white"  data-bg-img="nda-gallery/armyslide4.jpg">
  <div class="container">
    <div class="section-content">
      <div class="row">
        <div class="col-md-7">
          <h3 class="">Become a Member</h3>
          <div class="line-bottom mb-20"></div>
          <p class="mt-30 mb-30"><p><strong>Welcome to the Nigerian Defense Academy 38 Regular Course (NDA38RC)</p></strong>


We hope you enjoy visiting our website . The purpose of this platform is to reconnect and connect with the NDA community with a purpose to share experiences and initiatives.
</p>
          <a class="btn btn-colored btn-orange btn-lg pull-left pl-30 pr-30" href="{{URL::to('/register')}}">Join Us</a>
        </div>
        <div class="col-md-5">
          <div class="fluid-video-wrapper">
            <img class="pull-right"src="{{asset("nda-gallery/logo.png")}}" width="400"/>
          </div>
        </div>
      </div>
    </div>
  </div>      
</section>



<!-- end main-content -->
@stop