@extends("user.master")
@section("content")
<link rel="stylesheet" type="text/css" href="{{asset('vendor/lightbox2/dist/css/lightbox.min.css')}}">
<section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            <div class="col-xs-6 text-capitalize">
              {{$cat->category}}
            </div>
          </h3>
          <a href="" class="pull-right delete" title="delete">
             <i class="fa fa-trash fa-2x"></i>
          </a>
        </div>
        <div class="box-body">
         @foreach($images as  $image)
            <div class="col-xs-2">
              <a href="{{asset($image->url)}}" data-lightbox="{{$image->category->category}}">
              <div class="gallery-img-thumbnail" style="background-image:url({{asset($image->url)}})"></div>
              </a>
              <input type="checkbox" value="{{$image->id}}">
              @if($image->approved)
               <span class="badge bg-green">approved</span>
              @else
                <span class="badge bg-red">not approved</span>
              @endif
            </div>          
         @endforeach
        </div>
        <!-- /.box-body -->
        
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    {{Form::open(['route'=>'galleries.destroy','id'=>'deleteForm'])}}
    {{Form::close()}}
<script type="text/javascript" src="{{asset('vendor/lightbox2/dist/js/lightbox.min.js')}}"></script>
<script type="text/javascript">
  $(".delete").click(function(e)
  {
    e.preventDefault();
    if(!confirm('Are You Sure?'))
      return;
    var ch=$('input[type=checkbox]:checked');
    $('.delete-id').remove();
    var form=$("#deleteForm");

    ch.each(function(a,b)
    {
      b=$(b);
      console.log(b.val());
      var html=['<input class="delete-id" name="cats[]" type="hidden" value="'];
      html.push(b.val());
      html.push('">');
      form.append(html.join(''));
    })
    if(ch.length){
      form.submit();
    }
    console.log(ch.length);
  });
</script>
@stop