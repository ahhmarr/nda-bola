@extends("user.master")
@section("content")
<section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            <div class="col-xs-6">
              Gallery
            </div>
            
          </h3>
          <div class="row">
            <div class="col-xs-10">
              @include('galleries.createForm')  
            </div>
            
          </div>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <!-- <i class="fa fa-minus"></i></button> -->
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <!-- <i class="fa fa-times"></i></button> -->
          </div>
        </div>
        <div class="box-body">
         @foreach($images as  $image)
            <div class="col-xs-2">
              <div class="img-circle gallery-thumbnail" style="background-image:url({{asset($image->url)}})"></div>
              <div class="text-center text-capitalize">
              @if($image->category)
                {{$image->category->category}}
              @endif
              </div>
              <div class="text-center">
                <a href="{{route('gallery.category',$image->category_id)}}">({{$image->total}})</a>
              </div>
            </div>          
         @endforeach
        </div>
        <!-- /.box-body -->
        
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
@stop