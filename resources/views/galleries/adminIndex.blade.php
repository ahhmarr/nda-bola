@extends("admin.master")
@section("content")
<section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            <div class="col-xs-6">
              Gallery
            </div>
            
          </h3>
          <div class="row">
            <div class="col-xs-9">
            </div>
            <div class="col-xs-6 col-md-9">
             <span class="pull-right">

             </span>
            </div>
          </div>
          <div class="box-tools pull-right">
            
              <!-- <i class="fa fa-minus"></i></button> -->
             
          </div>
        </div>
        <div class="box-body">
          <!-- Start creating your amazing application! -->
           <table class="table">
             <thead>
               <tr>
                 <th>#</th>
                 <th>category</th>
                 <th>pending approval</th>
                 <th>Action</th>
               </tr>
             </thead>
             <tbody>
               @foreach($cats as $cat)
                
                <tr>
                  <td>{{@++$x}}</td>
                 
                  <td>{{$cat->category}}</td>
                  <td>
                    @if($cat->pending<=0)
                      <span class="badge bg-green">N/A</span>
                    @else
                      <span class="badge bg-red">{{$cat->pending}}</span>
                    @endif
                    
                  </td>
                  <td>
                    <a href="{{route('admin.categories.galleries',$cat->id)}}">View Images</a>
                  </td>
                </tr>
               @endforeach
             </tbody>
           </table>
        </div>
        <!-- /.box-body -->
        
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
@stop