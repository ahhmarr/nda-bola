{{Form::open(['route'=>'user.galleries.addImages'])}}
	<div class="col-xs-5">
		<select required name="category" class="form-control">
        <option value="">category</option>
        @foreach($cats as  $cat)
          <option value="{{$cat->id}}">{{$cat->category}}</option>
        @endforeach
      </select>
	</div>
	<div class="col-xs-4">
      <input type="submit" value="Add Images" class="btn btn-success">  
    </div>
{{Form::close()}}