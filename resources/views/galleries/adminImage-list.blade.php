@extends("admin.master")
@section("content")
<link rel="stylesheet" type="text/css" href="{{asset('vendor/lightbox2/dist/css/lightbox.min.css')}}">
<section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            <div class="col-xs-6 text-capitalize">
              {{$cat->category}}
            </div>
          </h3>
          <a href="" class="pull-right delete" title="Approve Selected">
             <i class="fa fa-check fa-2x"></i>
          </a>
        </div>
        <div class="box-body">
         @foreach($images as  $image)
            <div class="col-xs-2">
              <a href="{{asset($image->url)}}" data-lightbox="{{$image->category->category}}">
              <div class="gallery-img-thumbnail" style="background-image:url({{asset($image->url)}})"></div>
              </a>
              <input type="checkbox" value="{{$image->id}}">
              @if($image->approved)
                <a href="{{route('admin.galleries.approve',$image->id)}}">
                  <span class="badge bg-green">approved</span>
                </a>
               
              @else
                <a href="{{route('admin.galleries.approve',$image->id)}}">
                  <span class="badge bg-red">not approved</span>
                </a>
              @endif
            </div>          
         @endforeach
        </div>
        <!-- /.box-body -->
        
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    {{Form::open(['method'=>'get','route'=>'admin.galleries.approveBulk','id'=>'deleteForm'])}}
    {{Form::close()}}
<script type="text/javascript" src="{{asset('vendor/lightbox2/dist/js/lightbox.min.js')}}"></script>
<script type="text/javascript">
  $(".delete").click(function(e)
  {
    e.preventDefault();
    if(!confirm('Are You Sure?'))
      return;
    var ch=$('input[type=checkbox]:checked');
    $('.gallery-id').remove();
    var form=$("#deleteForm");

    ch.each(function(a,b)
    {
      b=$(b);
      console.log(b.val());
      var html=['<input class="gallery-id" name="gals[]" type="hidden" value="'];
      html.push(b.val());
      html.push('">');
      form.append(html.join(''));
    })
    if(ch.length){
      form.submit();
    }
    console.log(ch.length);
  });
</script>
@stop