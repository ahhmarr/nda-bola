@extends("user.master")
@section("content")
<link rel="stylesheet" type="text/css" href="{{asset('vendor/dropzone/dist/min/dropzone.min.css')}}">
<section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
             Add Images in <strong>{{$cat->category}}</strong>
          </h3>
         
        </div>

        {{Form::open(['role'=>'form','files'=>true,'route'=>'user.galleries.store','class'=>'dropzone'])}}
              <div class="box-body">
                {{Form::hidden('category',$cat->id)}}             
              </div>
        {{Form::close()}}
        <!-- /.box-body -->
        
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <script type="text/javascript" src="{{asset('vendor/dropzone/dist/min/dropzone.min.js')}}"></script>
    <script type="text/javascript">
  $(function(){
    Dropzone.options.dropzone={
    maxFiles : 100,
    acceptedFiles : 'image/png,image/jpg,image/jpeg',
    init : function()
    {
      this.on('success',function(obj,msg)
      {
        console.log('images uploaded');   
      })
    }
  };
  
  });
</script>
@stop