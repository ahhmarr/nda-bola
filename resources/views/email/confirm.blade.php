
<h3 style="color:#119448;">Thanks for Registration.</h3>

	<p>
		<b>To activate your account and verify your e-mail
		   address, please click on the following link: <a target="_blank" href="{{$link}}">
		   click here </a>
		</b>
	</p>
			<p>If clicking the link above does not work, copy and paste the URL in a
			new browser window instead.
			</p>

<p>{{$link}}</p>

<p>Thank you.</p>

<br />
<br />

	<p>This is a post-only mailing.  Replies to this message are not monitored
	or answered.
	</p>
