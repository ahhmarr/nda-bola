@extends("user.master")
@section("content")
<section class="content">
  <div class="row">
    <div class="col-xs-6 col-xs-offset-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="{{asset($user->details->avatar?:'img/user.png')}}" alt="User profile picture">

              <h3 class="profile-username text-center text-capitalize">
                {{full_name($user->details)}}
              </h3>

              <p class="text-muted text-center text-capitalize">
                {{userType($user->user_type)}}
              </p>

              <ul class="text-capitalize list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Name</b> <a class="pull-right">{{full_name($user->details)}}</a>
                </li>
                <li class="list-group-item">
                  <b>Gender</b> <a class="pull-right">{{gender($user->details->gender)}}</a>
                </li>
                <li class="list-group-item">
                  <b>User Type</b> <a class="pull-right">
                    {{userType($user->details->user_type_id)}}
                  </a>
                </li>
              </ul>

              <a href="{{route('user.edit')}}" class="btn btn-primary btn-block">
              <i class="fa fa-pencil"></i> <b>Edit</b>
              </a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- <strong><i class="fa fa-book margin-r-5"></i> Education</strong> -->

              <p class="text-muted">
                {{$user->details->about_you}}
              </p>

              <hr>

              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
  </div>
</section>
      
@stop