@extends("admin.master")
@section("content")
<section class="content">
	<div class="box">
            <div class="box-header">
              <h3 class="box-title">User</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody><tr>
                  <th style="width: 10px">#</th>
                  <th>name</th>
                  <th>Email</th>
                  <th>gender</th>
                  <th>type</th>
                  <th>verified</th>
                  <th>Approved</th>
                  <th>Action</th>
                </tr>
                <?php $x=0 ?>
                @foreach($users as  $us)
                	<tr>
                		<td>{{++$x}}</td>
                    <td>{{full_name($us->details)}}</td>
                		<td>{{$us->email}}</td>
                		<td>{{gender($us->details->gender)}}</td>
                		<td>{{userType($us->details->user_type_id)}}</td>
                		<td>
                			@if($us->confirm)
                			<span class="badge bg-green">yes</span>
                			@else
                			<span class="badge bg-red">no</span>
                			@endif
                		</td>
                		<td>
                			@if($us->allowed)
                			<span class="badge bg-green">yes</span>
                			@else
                      <a href="{{url('admin/user/approve/'.$us->id)}}">
                			<span class="badge bg-red">no</span>
                      </a>
                			@endif
                		</td>
                    <td></td>
                	</tr>
                @endforeach
                
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>   
</section>
{{$users->links()}}
@stop