<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset($user->details->avatar?:'img/user.png')}}" class="img-circle" alt="User Image" style="height: 60px">
        </div>
        <div class="pull-left info text-capitalize">
          <p class="text-capitalize">{{full_name($user->details)}}</p>
          <a href="#">
            {{userType($details->user_type_id)}}
          </a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="{{route('user.dashboard')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
          </a>
        </li>
        <li class="active treeview">
          <a href="{{route('user.profile')}}">
            <i class="fa fa-user"></i> <span>Profile</span> 
          </a>
        </li>
        <li class="active treeview">
          <a href="{{route('user.news.index')}}">
            <i class="fa fa-newspaper-o"></i> <span>News</span> 
          </a>
          <a href="{{route('user.articles.index')}}">
            <i class="fa fa-file-pdf-o"></i> <span>Articles</span> 
          </a>
          <a href="{{route('user.galleries.index')}}">
            <i class="fa  fa-file-image-o"></i> <span>Gallery</span>
          </a>
          <a href="{{route('user.categories.index')}}">
            <i class="fa  fa-file-text-o"></i> <span>Category</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>