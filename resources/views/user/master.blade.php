@include('user.header')
  <!-- Left side column. contains the logo and sidebar -->
  @include('user.sidebar')
  @include('scripts')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield("content")    
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      
    </div>
    <strong>
  </footer>

 {{-- @include('admin.control-sidebar') --}}
  
</div>
<!-- ./wrapper -->
@include('user.footer')
