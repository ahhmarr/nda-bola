@extends("user.master")
@section("content")
<section class="content">
  <div class="row">
    <div class="col-xs-6 col-xs-offset-3">
      <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Profile</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route'=>'user.update','files'=>true]) !!}
              <div class="box-body">
                <div class="form-group">
                  <label for="f_name">First Name</label>
                  <input type="text" required class="form-control" id="f_name" name="f_name" placeholder="Enter First Name"
                  value="{{$details->f_name}}">
                </div>
                <div class="form-group">
                  <label for="l_name">Last Name</label>
                  <input type="text" name="l_name" class="form-control" id="l_name" placeholder="Enter Last Name"
                  value="{{$details->l_name}}">
                </div>
                <div class="form-group">
                  <label for="password">Change Password</label>
                  <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                </div>
                <div class="form-group">
                  <label for="avatar">Avatar</label>
                  <input type="file" accept="image/x-png, image/gif, image/jpeg" id="avatar" name="avatar">
                </div>
                <div class="form-group">
                <label>User Type</label>  
                <div class="radio">
                  
                  <label>
                    <input type="radio" name="user_type" {{$details->user_type_id==1?'checked':''}} value="1"> Army
                  </label>
                  <label>
                    <input type="radio" name="user_type" {{$details->user_type_id==2?'checked':''}} value="2"> Navy
                  </label>
                  <label>
                    <input type="radio" name="user_type" {{$details->user_type_id==3?'checked':''}} value="3"> Air Force
                  </label>
                </div>
                </div>
                
                <div class="form-group">
                  <label for="about_you">About You</label>
                  <textarea class="editor form-control" id="about_you" placeholder="about you" name="about_you" rows="5">{{$details->about_you}}</textarea>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-block btn-warning">
                <i class="fa fa-save"></i> Upate</button>
              </div>
            {!! Form::close() !!}
          </div>
    </div>
  </div>
</section>
     
@stop