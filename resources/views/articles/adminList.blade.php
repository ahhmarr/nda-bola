@extends("admin.master")
@section("content")
<section class="content">
	<div class="box">
            <div class="box-header">
              <h3 class="box-title">Articles</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody><tr>
                  <th style="width: 10px">#</th>
                  <th>topic</th>
                  <th>user</th>
                  <th>description</th>
                  <th>last updated</th>
                  <th>Approved</th>
                  <th>Action</th>
                </tr>
                <?php $x=0 ?>
                @foreach($articles as  $article)
                	<tr>
                		<td>{{++$x}}</td>
                    <td>{{$article->topic}}</td>
                		<td>{{full_name($article->user->details)}}</td>
                		<td>{!!substr_close_tags($article->description,6)!!}</td>
                		<td>{{$article->updated_at->diffForHumans()}}</td>
                		<td>
                			@if($article->approved)
                			<a href="{{route('admin.articles.approve',$article->id)}}">
                        <span class="badge bg-green">yes</span>  
                      </a>
                			@else
                      <a href="{{route('admin.articles.approve',$article->id)}}">
                			<span class="badge bg-red">no</span>
                      </a>
                			@endif
                		</td>
                		<td>
                    <a target="_blank" href="{{route('admin.articles.show',$article->id)}}">
                      View
                    </a>  
                    </td>
                	</tr>
                @endforeach
                
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>   

</section>
{{$articles->links()}}
@stop