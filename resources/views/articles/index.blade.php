@extends("user.master")
@section("content")
<section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            <div class="col-xs-6">
              Articles
            </div>
            
          </h3>
          <div class="row">
            <div class="col-xs-3">
              <a href="{{route('user.articles.create')}}" class="btn btn-block btn-info">Create</a>
            </div>
            <div class="col-xs-9">
             <span class="pull-right">
                {{$articles->links()}}
             </span>
            </div>
          </div>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <!-- <i class="fa fa-minus"></i></button> -->
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <!-- <i class="fa fa-times"></i></button> -->
          </div>
        </div>
        <div class="box-body">
          <!-- Start creating your amazing application! -->
          <table class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Topic</th>
                <th>Description</th>
                <th>last updated</th>
                <th>Approved</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($articles as $article)
                <tr>
                  <td>{{@++$x}}</td>
                  <td>{{$article->topic}}</td>
                  <td>{!! substr_close_tags($article->description,20) !!}</td>
                  <td>{{$article->updated_at->diffForHumans()}}</td>
                  <td>
                    @if(!$article->approved)
                      <span class="badge bg-red">No</span>
                    @else
                      <span class="badge bg-green">
                        Yes
                      </span>
                    @endif
                  </td>
                  <td>
                    <div class="btn-group">
                      <button class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                        Action <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li> 
                          <a href="{{route('user.articles.show',$article->id)}}">
                          <i class="fa fa-user"></i> view
                          </a>
                        </li>
                        <li>
                          
                          <a href="{{route('user.articles.edit',$article->id)}}">
                            <i class="fa fa-pencil"></i> edit
                          </a>
                        </li>
                        <li>
                          <a href="">
                            <i class="fa fa-trash"></i> delete
                          </a>
                        </li>
                      </ul>
                    </div>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
@stop