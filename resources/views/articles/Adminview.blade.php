@extends("admin.master")
@section("content")
<section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h1 class="box-title text-capitalize">
               {{$article->topic}}

          </h1>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <!-- <i class="fa fa-times"></i></button> -->
          </div>
        </div>
        <div class="box-body">
            <p class="help-block">Last Updated - {{$article->updated_at->format('M d Y h:m')}}</p>
           <blockquote>
             {!! $article->description !!}
           </blockquote>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          @if($article->link)
            <a target="_blank" href="{{$article->link}}">
              {{$article->link}}
            </a>
          @endif
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      <div class="box box-solid">
            <div class="box-header with-border">
              
              <div class="col-xs-6">
                
                  @if($article->approved)
                  <a href="{{route('admin.articles.approve',$article->id)}}" class="confirm btn btn-block btn-danger">
                  <i class="fa fa-remove"></i> Disapprove
                  </a>
                  @else
                  <a href="{{route('admin.articles.approve',$article->id)}}" class="confirm btn btn-block btn-success">
                  <i class="fa fa-check"></i> Approve
                  </a>
                  @endif
                </a>
              </div>
            </div>
            
          </div>
    </section>
    <script type="text/javascript">
     /* $(function(){
        $("#description").wysihtml5();
      });*/
    </script>
@stop