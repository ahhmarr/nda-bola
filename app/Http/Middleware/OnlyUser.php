<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;
class OnlyUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=\Auth::user();
        if(!$user){
            return Redirect::to('/');
        }
        if($user->user_type==5) //ie shouldnt be admin
        {
            return Redirect::to('/admin/welcome');
        }
        return $next($request);
    }
}
