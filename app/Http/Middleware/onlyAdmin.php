<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;
class onlyAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=Auth::user();
        if(!$user){
            return Redirect::url('/');
        }
        if($user->user_type!=5) //ie shouldnt be admin
        {
            return Redirect::to('/user/welcome');
        }
        return $next($request);
    }
}
