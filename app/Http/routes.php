<?php


Route::get('/', function () {
    return view('site.index');
});
Route::get('/about', function () {
    return view('site.about');
});
Route::get('/contact', function () {
    return view('site.contact');
});
Route::get('/register', function () {
    return view('site.register');
});
Route::get('/login', function () {
    // dd(bcrypt(1));
    return view('site.login');
});
Route::get('/confirm', 'AuthCustomController@confirmMessage');
// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('/register', 'AuthCustomController@create');
//Route::auth();

Route::get('/home', 'HomeController@index');
Route::get('/contact', function () {
    return view('site.contact');
});
Route::get('/confirm-email/{code}','AuthCustomController@confirmEmail');
Route::post('/authenticate','AuthCustomController@authenticate');

//user routes
Route::group(['middleware'=>'App\Http\Middleware\OnlyUser','prefix'=>'/user'],function()
{
    Route::get('/welcome','UserController@index');
    Route::get('/dashboard',[
        'as' => 'user.dashboard',
        'uses' => 'UserController@index'
        ]);
	Route::get('/profile',[
        'as'=>'user.profile','uses'=>'UserController@viewProfile'
        ]);
    Route::get('/profile/edit',[
        'as'=>'user.edit','uses'=>'UserController@edit'
        ]);
    Route::post('/profile/update',[
        'as'=>'user.update','uses'=>'UserController@update'
        ]);
    //news 
    //activity
    //gallery
    Route::resource('news','NewsController');
    Route::get('/user/news/{id}/delete',
        ['as'=>'user.news.delete','NewsController@destroy']);
    Route::resource('articles','ArticlesController');
    Route::get('/user/articles/{id}/delete',
        ['as'=>'user.articles.delete','ArticlesController@destroy']);
    Route::resource('galleries','GalleriesController');
    Route::post('/user/galleries/create',
        ['as'=>'user.galleries.addImages','uses'=>'GalleriesController@create']);
    Route::resource('categories','CategoriesController');
    Route::get('/user/categories/{id}/delete',
        ['as'=>'user.categories.delete','uses'=>'CategoriesController@destroy']);
    Route::get('/user/category/{id}/galleries',
        ['as'=>'gallery.category','uses'=>'GalleriesController@showGalleryCategoryWise']);
    Route::post('/user/galleries/destroy',
        ['as'=>'galleries.destroy','uses'=>'GalleriesController@Destroy']);
});
Route::group(['middleware'=>'App\Http\Middleware\OnlyAdmin','prefix'=>'/admin'],function()
{
	Route::get('/welcome','AdminController@index');
	Route::get('/users',['as'=>'users.index','uses'=>'AdminController@users']);
	Route::get('/user/approve/{id}','AdminController@approve');
    Route::get('/news',
        ['as'=>'admin.news','uses'=>'NewsController@adminNews']);
    Route::get('/news/approve-disapprove/{id}',
        ['as'=>'admin.news.approve','uses'=>'NewsController@adminApprove']);
    Route::get('/news/{id}',
        ['as'=>'admin.news.show','uses'=>'NewsController@adminShow']);

    Route::get('/articles',
        ['as'=>'admin.articles','uses'=>'ArticlesController@adminNews']);
    Route::get('/articles/approve-disapprove/{id}',
        ['as'=>'admin.articles.approve','uses'=>'ArticlesController@adminApprove']);
    Route::get('/articles/{id}',
        ['as'=>'admin.articles.show','uses'=>'ArticlesController@adminShow']);
    Route::get('/galleries',
        ['as'=>'admin.galleries.index','uses'=>'GalleriesController@adminIndex']);
    Route::get('/categories/{id}/galleries',
        ['as'=>'admin.categories.galleries','uses'=>'GalleriesController@adminGalleryCategoryWise']);
    Route::get('/galleries/approve-disapprove/{id}',
        ['as'=>'admin.galleries.approve','uses'=>'GalleriesController@adminApprove']);
    Route::get('/galleries/approve-bulk',
        ['as'=>'admin.galleries.approveBulk','uses'=>'GalleriesController@adminApproveBulk']);

});

Route::get('/members','MembersController@index');
Route::get('/members/{type}','MembersController@index');
Route::get('/members{members-details}','MembersController@details');

Route::get('/articles',[
    'as'=>'site.articles.index','uses'=>'SiteArticleController@index']);
Route::get('/article/{id}',
    ['as'=>'site.article.show','uses'=>'SiteArticleController@show']);

Route::get('/gallery','SiteGalleryController@index');
