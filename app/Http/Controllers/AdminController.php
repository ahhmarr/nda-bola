<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Redirect;
use App\Http\Requests;
use App\User;
use App\Http\Controllers\BaseController;
class AdminController extends BaseController
{
    public function index()
    {
    	$user=$this->user;
        return view('admin.index',compact('user'));
    }

    public function users()
    {
    	// dd(bcrypt(1));
        $users=User::where('user_type','!=',5)->orderBy('id','DESC')->paginate(20);
    	return view('admin.user',compact('users'));
    }

    public function approve($id)
    {
    	$u=User::findOrFail($id);
    	$newpassword=substr(md5(time()),0,8);
    	$u->password=bcrypt($newpassword);
    	$u->allowed=1;
    	$u->save();
    	$this->sendVerificationMail($u,$newpassword);
        return Redirect::route('users.index');
    	
    }
    public function sendVerificationMail($u,$newpassword)
    {
    	$referer=\Request::root();
        $link=$referer.'/login';
    	Mail::send('email.approved',compact('u','newpassword','link'),function($message) use($u)
        {
            $message->to($u->email,$u->details->f_name)->subject('You have been approved');
        });
        return true;
    }
}
