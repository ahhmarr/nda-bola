<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Category;

class CategoriesController extends BaseController
{

    function index()
    {
        $cats=Category::orderBy('category','ASC')->get();
        $parents=Category::orderBy('category','ASC')->get();
        return view('categories.index',compact('parents','cats'));
    }


    function store(Request $req)
    {
        $cat=new Category;
        $cat->category=$req->input('category');
        $cat->parent_id=$req->input('parent');
        $cat->save();
        
        return redirect()->route('user.categories.index');
    }

    

    function update($id,Request $req)
    {
        $cat=Category::findOrFail($id);
        $cat->category=$req->input('category');
        $cat->parent_id=$req->input('parent');
        $cat->save();
        
        return redirect()->route('user.categories.index');
    }

    public function destroy($id)
    {
        $category=Category::findOrFail($id);
        $category->delete();
        
        return redirect()->route('user.categories.index');
    }
}
