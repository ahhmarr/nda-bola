<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\UserDetail;

class SiteGalleryController extends BaseController
{
    
    public function index()
    {
            return view('site.gallery.gallery');
    }

	public function show()
    {
        return view('site.article.show');
    }

}

