<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Article;

class ArticlesController extends BaseController
{

    function index()
    {
    	$articles=Article::orderBy('updated_at','DESC')->whereUserId($this->user->id)
                    ->paginate(30);

        return view('articles.index',compact('articles'));
    }

    function show($id)
    {
        $article=Article::findOrFail($id);
    	return view('articles.view',compact('article'));
    }

    function create()
    {
        return view('articles.create');
    }

    function store(Request $req)
    {
        ini_set('upload_max_filesize','5M');
        $article=new Article;
        $article->topic=$req->input('topic');
        $article->link=$req->input('link');
        $article->description=$req->input('description');
        $article->user_id=$this->user->id;
        $article->topic=$req->input('topic');
        $article->topic=$req->input('topic');
        $file=$req->file('file');
        if($file){
            $folder='docs/';
            $name=md5(time()).$file->getClientOriginalName();
            $file->move($folder,$name);
            $article->url=$folder.$name;
        }
        $article->save();
        return redirect()->route('user.articles.index');
    }

    function edit($id)
    {
        $article=Article::findOrFail($id);
    	return view('articles.edit',compact('article'));
    }

    function update($id,Request $req)
    {
        ini_set('upload_max_filesize','5M');
        $article=Article::findOrFail($id);
        $article->topic=$req->input('topic');
        $article->link=$req->input('link');
        $article->description=$req->input('description');
        $article->user_id=$this->user->id;
        $article->topic=$req->input('topic');
        $article->topic=$req->input('topic');
        $file=$req->file('file');
        if($file){
            $folder='docs/';
            $name=md5(time()).$file->getClientOriginalName();
            $file->move($folder,$name);
            $article->url=$folder.$name;
        }
        $article->save();
        return redirect()->route('user.articles.index');
    }

    public function destroy($id)
    {
        $article=Article::findOrFail($id);
        if($id->user_id==$this->user->id){
            $article->delete();
        }
        return redirect()->route('user.articles.index');
    }

    public function adminNews()
    {
        $articles=Article::orderBy('approved','ASC')
                    ->paginate(30);
        return view('articles.adminList',
            compact('articles'));
    }

    public function adminApprove($id)
    {
        $article=Article::findOrFail($id);
        if(!$article->approved){
            $article->approved=new \Carbon\Carbon;
        }else{
            $article->approved=null;
        }
        $article->save();
        return redirect()->back();
    } 

    public function adminShow($id)
    {
        $article=Article::findOrFail($id);
        return view('articles.Adminview',compact('article'));
    }
}
