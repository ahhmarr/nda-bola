<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\News;

class NewsController extends BaseController
{
    function index()
    {
    	$news=News::orderBy('updated_at','DESC')->whereUserId($this->user->id)
                    ->paginate(30);

        return view('news.index',compact('news'));
    }

    function show($id)
    {
        $new=News::findOrFail($id);
    	return view('news.view',compact('new'));
    }

    function create()
    {
        return view('news.create');
    }

    function store(Request $req)
    {
        $new=new News;
        $new->topic=$req->input('topic');
        $new->link=$req->input('link');
        $new->description=$req->input('description');
        $new->user_id=$this->user->id;
        $new->topic=$req->input('topic');
        $new->topic=$req->input('topic');
        $file=$req->file('file');
        if($file){
            $folder='docs/';
            $name=md5(time()).$file->getClientOriginalName();
            $file->move($folder,$name);
            $new->url=$folder.$name;
        }
        $new->save();
        return redirect()->route('user.news.index');
    }

    function edit($id)
    {
        $new=News::findOrFail($id);
    	return view('news.edit',compact('new'));
    }

    function update($id,Request $req)
    {
        $new=News::findOrFail($id);
        $new->topic=$req->input('topic');
        $new->link=$req->input('link');
        $new->description=$req->input('description');
        $new->user_id=$this->user->id;
        $new->topic=$req->input('topic');
        $new->topic=$req->input('topic');
        $file=$req->file('file');
        if($file){
            $folder='docs/';
            $name=md5(time()).$file->getClientOriginalName();
            $file->move($folder,$name);
            $new->url=$folder.$name;
        }
        $new->save();
        return redirect()->route('user.news.index');
    }

    public function destroy($id)
    {
        $new=News::findOrFail($id);
        if($id->user_id==$this->user->id){
            $new->delete();
        }
        return redirect()->route('user.news.index');
    }

    public function adminNews()
    {
        $news=News::orderBy('approved','ASC')
                    ->paginate(30);
        return view('news.adminList',
            compact('news'));
    }

    public function adminApprove($id)
    {
        $new=News::findOrFail($id);
        if(!$new->approved){
            $new->approved=new \Carbon\Carbon;
        }else{
            $new->approved=null;
        }
        $new->save();
        return redirect()->back();
    } 

    public function adminShow($id)
    {
        $new=News::findOrFail($id);
        return view('news.Adminview',compact('new'));
    }
}
