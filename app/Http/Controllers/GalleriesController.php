<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Category;
use App\Gallery;
use DB;
class GalleriesController extends BaseController
{

    function index()
    {
        $cats=Category::orderBy('category','ASC')->get();
        $images=$this->_getImagesCategoryWise();
        return view('galleries.index',compact('cats','images'));
    }
    function _getImagesCategoryWise(){
        $userID=$this->user->id;
        $images=Gallery::select(\DB::raw("count(DISTINCT id) as 'total',galleries.*"))
                        ->whereUserId($userID)
                        ->groupBy('category_id')
                        ->get();
        
        return $images;
    }
    function show($id)
    {
        $article=Article::findOrFail($id);
    	return view('articles.view',compact('article'));
    }

    function create(Request $req)
    {
        $id=$req->input('category');
        $cat=Category::findOrFail($id);
        
        return view('galleries.create',compact('cat'));
    }

    function store(Request $req)
    {
        $gallery=new Gallery;
        $cat=$req->input('category');
        $gallery->category_id=$cat;
        $gallery->user_id=$this->user->id;
        $file=$req->file('file');
        if($file){
            $folder='docs/';
            $name=md5(time()).$file->getClientOriginalName();
            $file->move($folder,$name);
            $gallery->url=$folder.$name;
        }
        $gallery->save();
    }

    function edit($id)
    {
        $article=Article::findOrFail($id);
    	return view('articles.edit',compact('article'));
    }

    function update($id,Request $req)
    {
        ini_set('upload_max_filesize','5M');
        $article=Article::findOrFail($id);
        $article->topic=$req->input('topic');
        $article->link=$req->input('link');
        $article->description=$req->input('description');
        $article->user_id=$this->user->id;
        $article->topic=$req->input('topic');
        $article->topic=$req->input('topic');
        $file=$req->file('file');
        if($file){
            $folder='docs/';
            $name=md5(time()).$file->getClientOriginalName();
            $name=str_replace(" ","_",$name);
            $file->move($folder,$name);
            $article->url=$folder.$name;
        }
        $article->save();
        return redirect()->route('user.articles.index');
    }

    public function destroy(Request $req)
    {
     $ids=$req->input('cats');
     Gallery::whereIn('id',$ids)->delete();
     return redirect()->back();
    }

    public function showGalleryCategoryWise($id)
    {
        $userID=$this->user->id;
        $cat=Category::findOrFail($id);
        $images=Gallery::whereCategoryId($id)
                        ->whereUserId($userID)
                        ->get();
        return view('galleries.image-list',compact('images','cat'));
    }

    function adminIndex()
    {
        $cats=Category::orderBy('category','ASC')->get();
        $this->_addPending($cats);
        return view('galleries.adminIndex', compact('cats'));
    }

    function _addPending(&$cats)
    {
        foreach ($cats as $cat) {
            $g=Gallery::whereNull('approved')->whereCategoryId($cat->id)->count();
            $cat->pending=$g;
        }
    }

    function adminGalleryCategoryWise($id)
    {
        $cat=Category::findOrFail($id);
        $images=Gallery::whereCategoryId($id)
                        ->get();
        return view('galleries.adminImage-list',compact('images','cat'));
    }

    function adminApprove($id,$null=false)
    {
        $gal=Gallery::findOrFail($id);
        if(!$gal->approved)
        {
            $gal->approved=new \Carbon\Carbon;
        }
        elseif(!$null){
            $gal->approved=null;
        }
        $gal->save();
        return redirect()->back();
    }

    function adminApproveBulk(Request $req)
    {
        $gals=$req->input('gals');
        foreach($gals as  $gal){
            $this->adminApprove($gal,1);
        }
        return redirect()->back();
    }

}
