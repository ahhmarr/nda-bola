<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\UserDetail;
use DB;
use App\Article;

class SiteArticleController extends BaseController
{
    
    public function index(Request $req)
    {
        $articles=Article::orderBy('created_at')
        					->orderBy('approved','DESC')
        					->whereNotNull('approved');

        $articles=$this->_filter($req,$articles);
        $articles=$articles->paginate(30);
        					
        $bars=$this->_getSideBar();
        return view('site.article.article',
        	compact('articles','bars'));
    }

	public function show($id)
    {
        $article=Article::findOrFail($id);
        $bars=$this->_getSideBar();
        return view('site.article.show',
            compact('article','bars'));
    }

    public function _getSideBar()
    {
        $bars=Article::select(DB::raw('count(id) as "total",articles.*,MONTH(created_at) as m,YEAR(created_at) as y'))
                ->groupBy(DB::raw('MONTH(created_at)'))
                ->groupBy(DB::raw('YEAR(created_at)'))
                ->orderBy('created_at','DESC')
                ->paginate(10);
        return $bars; 
    }

    function _filter($req,$articles)
    {
        $month=$req->input('m');
        $year=$req->input('y');
        if($month){
            $articles=$articles->whereRaw('MONTH(created_at)=?',[$month]);
        }
        if($year){
         $articles=$articles->whereRaw('YEAR(created_at)=?',[$year]);   
        }
        return $articles;
    }
}

