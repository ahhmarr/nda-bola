<?php

namespace App\Http\Controllers;

use Mail;
use Auth;
use DB;
use App\User;
use App\UserDetail;
use Validator;
use Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthCustomController extends Controller
{



    protected function validator(array $data)
    {
        return Validator::make($data, [
            'f_name'    => 'required|max:255',
            'email'     => 'required|email|max:255',
            'password'  => 'required|min:6|confirmed',
            'gender'    => 'required|integer', 
            'service_type'    => 'required|integer'
        ]);
    }

    protected function create(Request $request)
    {
        // dd('called');
        $data=[
            'f_name'    => $request->input('f_name'),
            'l_name'    => $request->input('l_name'),
            'about_you'    => $request->input('about_you'),
            'email'     => $request->input('email'),
            'password'  => $request->input('password'),
            'gender'    => $request->input('gender') ,
            'service_type'    => $request->input('service_type') 
        ];
        $type=$request->input('type'); //admin or other users
        \DB::beginTransaction();
        $user= User::create([
            'email' => $data['email'],
            'code'   => md5(time()),
            'password' => bcrypt($data['password']),
            'user_type'  => $type?:$data['service_type']
        ]);
        $details=UserDetail::create([
            'f_name'    => $data['f_name'],
            'l_name'    => $data['l_name'],
            'gender'    => $data['gender'] ,
            'about_you'    => $data['about_you'] ,
            'user_type_id'    => $data['service_type'],
            'user_id'       => $user->id
        ]);
        if($user && $details){
            DB::commit();
        }
        else{
            DB::rollback();
        }
        // send email for confirmation
     $this->_sendEmail($user,$user);
     return Redirect::to('/confirm')
            ->with('email',$user->email);
    }
    public function _sendEmail($user,$details)
    {
        $referer=\Request::root();

        $link=$referer.'/confirm-email/'.$user->code;

        Mail::send('email.confirm',compact('link'),function($message) use($user,$details)
        {
            $message->to($user->email,$details->f_name)->subject('confirmation');
        });
        // return view('email.confirm',compact('link'));
    }
    public function confirmMessage()
    {
        $email=\Session::get('email');
        return view('auth.confirm',compact('email'));
    }

    public function confirmEmail($code)
    {
        $user=User::whereCode($code)->first();
        if($user)
        {
            $user->confirm=1;
            $user->code=DB::raw('null');
            $user->save();    
            return view('auth.confirmWait',compact('user'));
        }
        else
        {
            return view('auth.invalid');
        }
        
    }

    public function authenticate(Request $request)
    {
        $email=$request->input('email');
        $password=$request->input('password');
        // dd($email);
        $auth=\Auth::attempt([
            'email' => $email,
            'password'  => $password
            ]);
        if($auth){
            $user=Auth::user();
            if($this->_checkIfNot($user,'confirm')){
                Auth::logout();
                return view('auth.notconfirmed');
            }
            elseif($this->_checkIfNot($user,'allowed')){
                Auth::logout();
                return view('auth.notallowed');
            }
            else
            {
                $user=Auth::user();
                $type=$this->_userTypeLocation($user->user_type);
                return Redirect::to($type.'/welcome');
            }
        }
        else
        {
            return view('auth.invalid');
        }
    }

    function _checkIfNot($user,$key)
    {
        return !$user->$key;
    }

    function _userTypeLocation($type)
    {
        $prefix='user';
        if($type=='5') {
            $prefix='admin';
        }
        return $prefix;
    }
}
