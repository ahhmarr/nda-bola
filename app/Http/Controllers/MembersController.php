<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\UserDetail;

class MembersController  extends BaseController
{
    
    public function index($type='')
    {
    	$users=User::whereIn('user_type',[1,2,3]);
    	if($type){
    		$users=$users->whereUserType($type);
    	}
    	$users=$users->paginate(15);
        return view('members.index',compact('users','type'));
    }

	public function details()
    {
        return view('members.members-details');
    }

}

