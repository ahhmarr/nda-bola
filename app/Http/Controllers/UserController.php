<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
class UserController extends BaseController
{
    function index()
    {
    	return view('user.index');
    }

    function viewProfile()
    {
    	return view('user.profile');
    }

    function edit()
    {
    	return view('user.edit');
    }

    function update(Request $req)
    {
    	$user=User::findOrFail($this->user->id);
    	$fName=$req->input('f_name');
    	$lName=$req->input('l_name');
    	$password=$req->input('password');
    	$type=$req->input('user_type');
    	$about=$req->input('about_you');
    	$file=$req->file('avatar');
    	if($file){
    		$url=md5(time()).$file->getClientOriginalName();
    		$file->move('docs/',$url);
    		$url='docs/'.$url;
    		$user->details->avatar=$url;
    	}
    	if($password)
    		$user->password=bcrypt($password);
    	$user->details->f_name=$fName;
    	$user->details->l_name=$lName;
    	$user->details->user_type_id=$type;
    	$user->user_type=$type;
    	$user->details->about_you=$about;
    	$user->save();
    	$user->details->save();
    	return redirect()->back();

    }
}
