<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use View;
use App\Http\Requests;

class BaseController extends Controller
{
    public function __construct()
    {
    	View::share("user","");
    	if(Auth::user()){
    		$this->user=Auth::user();
    		View::share("user",Auth::user());
    		View::share("details",Auth::user()->details);
    	}

    }
}
