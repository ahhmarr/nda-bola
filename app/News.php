<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class News extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'topic', 'user_id', 'link','url','description','approved'
    ];

    protected $table='news';

    protected $dates=['approved']   ;

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
