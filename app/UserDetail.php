<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $fillable=['f_name','l_name','email' ,'gender','about_you','service_type','user_id','user_type_id'  ];
}
